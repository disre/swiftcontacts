//
//  addContact.swift
//  SwiftContacts
//
//  Created by Kelson Vella on 10/12/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import RealmSwift

protocol contactDelegate {
    func passData(contact: Contact)
}

class addContact: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var contact: Contact?
    var delegate: contactDelegate?
    
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var fName: UITextField!
    @IBOutlet weak var lName: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var street2: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var state: UITextField!
    @IBOutlet weak var zip: UITextField!
    @IBOutlet weak var country: UITextField!
    
    @IBAction func selectImage(_ sender: UITapGestureRecognizer) {
        fName.resignFirstResponder()
        lName.resignFirstResponder()
        phone.resignFirstResponder()
        email.resignFirstResponder()
        street.resignFirstResponder()
        street2.resignFirstResponder()
        city.resignFirstResponder()
        state.resignFirstResponder()
        zip.resignFirstResponder()
        country.resignFirstResponder()
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func saveContact(_ sender: Any) {
        let realm = try! Realm()
        
        let imageData = UIImagePNGRepresentation(picture.image!)
        
        if contact == nil {
            try! realm.write {
                realm.add(Contact(name: fName.text!, lName:lName.text!, phone: phone.text!, email:email.text!, picture:imageData!, street: street.text!, street2: street2.text!, city: city.text!, state: state.text!, zip: zip.text!, country: country.text!))
            }
            
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            try! realm.write {
                contact?.name = fName.text!
                contact?.phone = phone.text!
                contact?.lName = lName.text!
                contact?.email = email.text!
                contact?.picture = imageData
                contact?.street = street.text!
                contact?.street2 = street2.text!
                contact?.city = city.text!
                contact?.state = state.text!
                contact?.zip = zip.text!
                contact?.country = country.text!
            }
            delegate?.passData(contact: contact!)
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        lName.delegate = self
        phone.delegate = self
        email.delegate = self
        street.delegate = self
        street2.delegate = self
        city.delegate = self
        state.delegate = self
        zip.delegate = self
        country.delegate = self
        
        if contact != nil{
            fName.text = contact?.name
            phone.text = contact?.phone
            lName.text = contact?.lName
            email.text = contact?.email
            picture.image = UIImage(data: contact!.picture!, scale: 1.0)
            street.text = contact?.street
            street2.text = contact?.street2
            city.text = contact?.city
            state.text = contact?.state
            zip.text = contact?.zip
            country.text = contact?.country
        }
        picture.layer.borderWidth = 1.0
        picture.layer.masksToBounds = false
        picture.layer.borderColor = UIColor.white.cgColor
        picture.layer.cornerRadius = picture.frame.size.width/2
        picture.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Wrong thing go this instead of a dictionary: \(info)")
        }
        picture.image = selectedImage
        picture.layer.borderWidth = 1.0
        picture.layer.masksToBounds = false
        picture.layer.borderColor = UIColor.white.cgColor
        picture.layer.cornerRadius = picture.frame.size.width/2
        picture.clipsToBounds = true
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
