//
//  ViewContact.swift
//  SwiftContacts
//
//  Created by Kelson Vella on 10/12/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit
import MessageUI
import RealmSwift
import CoreLocation
import MapKit

class ViewContact: UIViewController, contactDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate{
    
    var contact: Contact?
    let regionRadius: CLLocationDistance = 1000
    
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var fName: UILabel!
    @IBOutlet weak var phone: UIButton!
    @IBOutlet weak var email: UIButton!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var street2: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var zip: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet var map: MKMapView!
    
    @IBAction func viewMap(_ sender: UITapGestureRecognizer) {
        let placemark = MKPlacemark(coordinate: map.centerCoordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: nil)
    }
    
    @IBAction func callPhone(_ sender: Any) {
        if let url = URL(string: "tel://\(contact!.phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([contact!.email])
            present(mail, animated: true)
        } else {
            print("email error")
        }
    }
    @IBAction func sendText(_ sender: Any) {
        if MFMessageComposeViewController.canSendText() {
            let message = MFMessageComposeViewController()
            message.recipients = [contact!.phone]
            message.messageComposeDelegate = self
            present(message, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        picture.layer.borderWidth = 1.0
        picture.layer.masksToBounds = false
        picture.layer.borderColor = UIColor.white.cgColor
        picture.layer.cornerRadius = picture.frame.size.width/2
        picture.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fName.text = contact!.name + " " + contact!.lName
        phone.setTitle(contact!.phone, for: .normal)
        phone.contentHorizontalAlignment = .left
        email.setTitle(contact!.email, for: .normal)
        email.contentHorizontalAlignment = .left
        picture.image = UIImage(data: contact!.picture!, scale: 1.0)
        street.text = contact?.street
        street2.text = contact?.street2
        city.text = contact?.city
        state.text = contact?.state
        zip.text = contact?.zip
        country.text = contact?.country
//        let addressString = "\(street.text) \(street2.text) \(city.text) \(state.text) \(country.text) \(zip.text) "
        let addressString = street.text! + " " + street2.text! + " " + city.text! + " " + state.text! + " " + country.text! + " " + zip.text!
        getLocation(addressString: addressString)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit" {
            if let nextViewController = segue.destination as? addContact {
                nextViewController.contact = contact
                nextViewController.delegate = self
            }
        }
    }
    
    func passData (contact: Contact) {
        self.contact = contact
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true)
    }

    func centerMap(location: CLLocation) {
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        let point:MKPointAnnotation = MKPointAnnotation()
        point.coordinate = location.coordinate
        map.setRegion(region, animated: true)
        map.addAnnotation(point)
    }
    
    func getLocation( addressString : String){
        CLGeocoder().geocodeAddressString(addressString, completionHandler: {(placemarks, error) in
            if error != nil {
                print ("Geocoding the address failed: \(error!.localizedDescription)")
            } else if placemarks!.count > 0 {
                let placemark = placemarks![0]
                let location = placemark.location
                self.centerMap(location: location!)
            }
        })
    }
}
