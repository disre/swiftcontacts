//
//  Contact.swift
//  SwiftContacts
//
//  Created by Kelson Vella on 10/12/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import Foundation
import RealmSwift

class Contact: Object {
    dynamic var name = ""
    dynamic var lName = ""
    dynamic var phone = ""
    dynamic var email = ""
    dynamic var picture:Data?
    dynamic var street = ""
    dynamic var street2 = ""
    dynamic var city = ""
    dynamic var state = ""
    dynamic var zip = ""
    dynamic var country = ""
    
    convenience init (name: String, lName: String, phone: String, email: String, picture: Data, street: String, street2: String, city: String, state: String, zip: String, country: String) {
        self.init()
        self.name = name
        self.lName = lName
        self.phone = phone
        self.email = email
        self.picture = picture
        self.street = street
        self.street2 = street2
        self.city = city
        self.state = state
        self.zip = zip
        self.country = country
    }
}
